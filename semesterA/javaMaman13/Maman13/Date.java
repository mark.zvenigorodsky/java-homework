// A Date class that answers the requirements of Maman 13

public class Date{

	// Class parameters that define a Date
	int _day;
	int _month;
	int _year;
	
	//Boundaries and constants for the date inputs
	final int MAX_DAY= 31;
	final int MIN_DAY = 1;
	final int MAX_MONTH = 12; 
	final int MIN_MONTH = 1;
	final int MAX_YEAR = 9999;
	final int MIN_YEAR = 1000;
	final int MAX_DAY_IN_EVEN_MONTHS = 30;
	final int MAX_DAY_IN_UNEVEN_MONTHS  = 31;
	final int MAX_DAY_IN_FEBRUARY = 28;	
	final int ZERO = 0;
	final int DEFAULT_DAY = 1;
	final int DEFAULT_MONTH = 1;
	final int DEFAULT_YEAR = 2000;
	final int NEGATIVE = -1;
	final int FEBRUARY = 2;
	final int ONE = 1;
	final int TWO = 2;
	final int TEN = 10;
	final String ZERO_STRING = "0";

	// First constructor that accpets three integers that represent a date
	public Date(int day, int month, int year){
		if(!validateDate(day, month, year)){
			_day = DEFAULT_DAY;
			_month = DEFAULT_MONTH;
			_year = DEFAULT_YEAR;
			return;	
		}

		_day = day;
		_month = month;
		_year = year;
	}

	// Second constructor that recieves a Date object and sets itself to said Date
	public Date(Date other){	
		_day = other.getDay();
		_month= other.getMonth();
		_year = other.getYear();
		
	}

	// Method to get info about said Date
	public int getDay(){
		return this._day;
	}


	public int getMonth(){
		return this._month;
	}


	public int getYear(){
		return this._year;
	}
	
	// Methods to set info about said Date	
	public void setDay(int dayToSet){
		if(dayToSet > MAX_DAY || dayToSet < MIN_DAY) return;
		this._day = dayToSet;
	}

	public void setMonth(int monthToSet){
		if(monthToSet > MAX_MONTH || monthToSet < MIN_MONTH) return;
		this._month = monthToSet;
	}

	public void setYear(int yearToSet){
		if(yearToSet > MAX_YEAR || yearToSet < MIN_YEAR) return;
		this._year = yearToSet;
	}

	// Method that checks if the two Dates are the same
	public boolean equals(Date other){
		if(this._day == other._day && this._month == other._month && this._year == other._year) return true;
		return false;
	} 

	// Method that checks if the Date that called the method is before the other Date
	public boolean before(Date other){
		final int d1 = calculateDate(this._day, this._month, this._year);
		final int d2 = calculateDate(other._day, other._month, other._year);

		if(d1 < d2) return true;
		return false;
	} 


	// Method that checks if the Date that called the method is after the other Date
	public boolean after(Date other){
		if(this.before(other)) return false;
		return true;
	} 

	// Method that returns the difference between two dates in days	
	public int difference(Date other){
		final int d1 = calculateDate(this._day, this._month, this._year);
		final int d2 = calculateDate(other._day, other._month, other._year);

		int difference = d1 - d2;
	
		if(difference < ZERO) difference = difference * NEGATIVE;
		
		return difference;
	} 

	// Method that return the Date as a string
	public String toString(){
		String stringDay = "" + this._day;
		String stringMonth = "" + this._month;
	
		if(this._day < TEN) stringDay = ZERO_STRING + stringDay; 
		if(this._month < TEN) stringMonth = ZERO_STRING + stringMonth; 

		return stringDay + "/" + stringMonth + "/" + this._year;
	}

	// Method that returns a new Date object of tommorows Date	
	public Date tomorrow(){
		int day = this._day + ONE;
		int month = this._month;
		int year = this._year;

		if(!validateDayInMonth(day, this._month)){
			day = ONE;
			month += ONE;
			if(month == MAX_MONTH) {
				month = ONE;
				year += ONE;
			}
		}
		Date tommorow = new Date(day, month, year);
		return tommorow;
		
	}

	// Calculates the days since A.C. (don't need to define constants in this method according to instructions)
	private int calculateDate(int day, int month, int year){
		if(month < 3){
			year--;
			month = month + 12;
		}
		return 365 * year + year/4 - year/100 + year/400 + ((month+1) * 306)/10 + (day - 62);
	}


	// Validates The inserted Date
	private boolean validateDate(int day, int month, int year){
		if(day > MAX_DAY || day < MIN_DAY || month > MAX_MONTH || month < MIN_MONTH || year > MAX_YEAR || year < MIN_YEAR) return false;
		if(validateDayInMonth(day, month)) return true;
		return false;
	}

	private boolean validateDayInMonth(int day, int month){
		if(month == FEBRUARY)
			if(day < MAX_DAY_IN_FEBRUARY) return true;
		if((month % TWO) == ONE)
			if(day < MAX_DAY_IN_UNEVEN_MONTHS) return true;
		if((month % TWO) == ZERO)
			if(day < MAX_DAY_IN_EVEN_MONTHS) return true;
		return false;
	}
}
