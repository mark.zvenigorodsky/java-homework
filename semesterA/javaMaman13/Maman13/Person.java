// A Person class that answers the requirements of Maman 13

public class Person{
	
	// Class parameters that define a person
	String _name;
	String _id;
	Date _dateOfBirth;

	// Constants
	final String DEFAULT_NAME = "Someone";
	final String DEFAULT_ID = "000000000";
	final int ZERO = 0;
	final int NINE = 9;
	final int MAX_DAY= 31;
	final int MIN_DAY = 1;
	final int MAX_MONTH = 12; 
	final int MIN_MONTH = 1;
	final int MAX_YEAR = 9999;
	final int MIN_YEAR = 1000;
	final int MAX_DAY_IN_EVEN_MONTHS = 30;
	final int MAX_DAY_IN_UNEVEN_MONTHS  = 31;
	final int MAX_DAY_IN_FEBRUARY = 28;	
	final int FEBRUARY = 2;
	final int ONE = 1;
	final int TWO = 2;

	// First constructor that accepts arguments
	public Person(String name, int day, int month, int year, String id){
		if(!checkName(name)) _name = DEFAULT_NAME;	
		else _name = name;

		if(!checkId(id)) _id = DEFAULT_ID;
		else _id = id;

		_dateOfBirth = new Date(day, month, year);
	}

	// Second constructor that recieves another Person object 
	public Person(Person other){
		_name = other._name;
		_id = other._id;
		_dateOfBirth = other._dateOfBirth;
	}

	// Methods to get info about said Person
	public String getName(){
		return this._name;	
	}
	public String getId(){
		return this._id;	
	}
	public Date getDateOfBirth(){
		return this._dateOfBirth;
	}

	// Methods to set new info about said Person
	public void setName(String name){
		if(checkName(name)) this._name = name;
	}
	public void setId(String id){
		if(checkId(id)) this._id = id;
	}
	public void setDateOfBirth(Date d){
		int day = d.getDay();
		int month = d.getMonth();
		int year = d.getYear();

		if(validateDate(day, month, year)) this._dateOfBirth = d;
	}

	// Method that returns all of the said Persons info as a string
	public String toString(){
		return "Person name: " + this._name + "\nPerson id: " + this._id + "\nDate of birth: " + this._dateOfBirth;
	}
	
	// Method that Checks if the other Person is the same Person who called the method
	public boolean equals(Person other){
		if(this.toString().equals(other.toString())) return true;
		return false;
	}

	// Method that checks who is younger between the Person that called the method and another
	public int compare(Person other){
		Date d1 = this._dateOfBirth;
		Date d2 = other._dateOfBirth;

		if(d1.equals(d2))return 0;

		if(d1.before(d2))return  1;

		return -1;
	}

	// Methods that verify the name and id that are inserted 
	private boolean checkName(String name){
		if(name.length() == ZERO) return false;
		return true;
	}

	private boolean checkId(String id){
		if(id.length() != NINE) return false;
		return true;
	}

	// Validates that the date is in range and valid
	private boolean validateDate(int day, int month, int year){
		if(day > MAX_DAY || day < MIN_DAY || month > MAX_MONTH || month < MIN_MONTH || year > MAX_YEAR || year < MIN_YEAR) return false;
		if(validateDayInMonth(day, month)) return true;
		return false;
	}
	private boolean validateDayInMonth(int day, int month){
		if(month == FEBRUARY)
			if(day < MAX_DAY_IN_FEBRUARY) return true;
		if((month % TWO) == ONE)
			if(day < MAX_DAY_IN_UNEVEN_MONTHS) return true;
		if((month % TWO) == ZERO)
			if(day < MAX_DAY_IN_EVEN_MONTHS) return true;
		return false;
	}
}
