// An Apartment class that answers the requirements of Maman 13

public class Apartment{

	// Class parameters that define an Apartment
	int _noOfRooms;
	double _area;
	double _price;
	Person _tenant;
	Date _rentalStartDate;
	Date _rentalEndDate;


	// Boundaries and constants for the date inputs
	final int ZERO = 0;
	final int NINE = 9;
	final int MAX_DAY= 31;
	final int MIN_DAY = 1;
	final int MAX_MONTH = 12; 
	final int MIN_MONTH = 1;
	final int MAX_YEAR = 9999;
	final int MIN_YEAR = 1000;
	final int MAX_DAY_IN_EVEN_MONTHS = 30;
	final int MAX_DAY_IN_UNEVEN_MONTHS  = 31;
	final int MAX_DAY_IN_FEBRUARY = 28;	
	final int FEBRUARY = 2;
	final int ONE = 1;
	final int TWO = 2;
	final int DEFAULT_NUMBER_OF_ROOMS = 3;
	final double DEFAULT_PRICE= 80;
	final double DEFAULT_AREA= 5000;
	final double NINTY = 90;

	// First Constructor for Apartment that accepts the following parameters:
	// number of rooms, area, price, tenant, and integers that represent a start and end date.
	public Apartment(int noOfRooms, double area, double price, Person p, int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear){
		if(!isPositive(noOfRooms)) _noOfRooms = DEFAULT_NUMBER_OF_ROOMS;
		else _noOfRooms = noOfRooms;

		if(!isPositive(area)) _area = DEFAULT_AREA;
		else _area = area;

		if(!isPositive(price)) _price = DEFAULT_PRICE;
		else _price = price;

		_tenant = new Person(p);

		_rentalStartDate = new Date(startDay, startMonth, startYear);

		_rentalEndDate = new Date(endDay, endMonth, endYear);

		if(_rentalEndDate.before(_rentalStartDate) || _rentalEndDate.equals(_rentalStartDate)) _rentalEndDate = new Date(startDay, startMonth, startYear + ONE);
		
	}
	// A second constructor that accpets another Apartment object and clones it
	public Apartment(Apartment other){
		_noOfRooms = other._noOfRooms;
		_area = other._area;
		_price = other._price;
		_rentalStartDate = other._rentalStartDate;
		_rentalEndDate = other._rentalEndDate;
		_tenant = other._tenant;
	}

	// Method that changes tanants only if the new tenant is youger, pays the same or more, and his rental start 
	// day is 90 days away from the rental end date of the current tenant.
	public boolean changeTenant(Date startRent, Person p, double price){
		if(this._tenant.getDateOfBirth().before(p.getDateOfBirth()) && this._price <= price && startRent.before(this._rentalEndDate) && startRent.difference(this._rentalEndDate) <= NINTY){
			Date endRent = new Date(startRent);
			endRent.setYear(startRent.getYear() + ONE);
			this.setTenant(p);
			this.setPrice(price);
			this.setRentalStartDate(startRent);
			this.setRentalEndDate(endRent);

			return true;
		}
		return false;
	}
	
	// Returns the amount of days left to the rental end date.
	public int daysLeft(Date d){
		if(this._rentalEndDate.before(d)) return -1;

		return this._rentalEndDate.difference(d);
	}
	
	// Compares two Apartment objects to check if they're the same one.
	public boolean equals(Apartment other){
		if(this.toString().equals(other.toString())) return true;
		return false;
	}

	// Method that extends the rental date by the years given.
	public void extendRentalPeriod(int years){
		if(!isPositive(years) || (this._rentalEndDate.getYear() + years) > MAX_YEAR) return;

		Date newEndDate = new Date(this._rentalEndDate.getDay(), this._rentalEndDate.getMonth(), this._rentalEndDate.getYear() + years);
		this.setRentalEndDate(newEndDate);
	}

	// Methods to get data about the Apartment object
	public double getArea(){
		return this._area;
	}

	public int getNoOfRooms(){
		return this._noOfRooms;
	}

	public double getPrice(){
	 	return this._price;
	}

	public Date getRentalEndDate(){
		return this._rentalEndDate;
	}

	public Date getRentalStartDate(){
		return this._rentalStartDate;
	}

	public Person getTenant(){
		return this._tenant;
	}

	// Methods to set data about the Apartment object
	public void setArea(double area){
		if(isPositive(area)) this._area = area;
	}


	public void setNoOfRooms(int noOfRooms){
		if(isPositive(noOfRooms)) this._noOfRooms = noOfRooms;
	}

	public void setPrice(double price){
		if(isPositive(price)) this._price = price;
	}

	public void setRentalEndDate(Date rentalEndDate){
		int day = rentalEndDate.getDay();
 		int month = rentalEndDate.getMonth();
		int year = rentalEndDate.getYear();

		if(validateDate(day, month, year)) this._rentalEndDate = rentalEndDate;
	}

	public void setRentalStartDate(Date rentalStartDate){
		int day = rentalStartDate.getDay();
 		int month = rentalStartDate.getMonth();
		int year = rentalStartDate.getYear();

		if(validateDate(day, month, year)) this._rentalStartDate = rentalStartDate;
	}

	public void setTenant(Person tenant){
		int day = tenant.getDateOfBirth().getDay();
 		int month = tenant.getDateOfBirth().getMonth();
		int year = tenant.getDateOfBirth().getYear();

		if(checkName(tenant.getName()) && checkId(tenant.getId()) && validateDate(day, month, year)){
			this._tenant = tenant;
		}
	}

	// Returns the Apartment object's data as a string
	public String toString(){
		return "Nubmer of rooms: " + this._noOfRooms + "\nArea: " + this._area + "\nPrice: " + this._price + "\nTenant name: " + this._tenant.getName() + "\nRental start date: " + this._rentalStartDate.toString() + "\nRental end date: " + this._rentalEndDate.toString();
	}

	// Private Methods to check if the number is positive
	private boolean isPositive(int arg){
		if(arg <= 0) return false;
		return true;
	}


	private boolean isPositive(double arg){
		if(arg <= 0) return false;
		return true;
	}

	// Methods that verify the name and id that are inserted 
	private boolean checkName(String name){
		if(name.length() == ZERO) return false;
		return true;
	}

	private boolean checkId(String id){
		if(id.length() != NINE) return false;
		return true;
	}

	// Validates that the date is in range and valid
	private boolean validateDate(int day, int month, int year){
		if(day > MAX_DAY || day < MIN_DAY || month > MAX_MONTH || month < MIN_MONTH || year > MAX_YEAR || year < MIN_YEAR) return false;
		if(validateDayInMonth(day, month)) return true;
		return false;
	}
	private boolean validateDayInMonth(int day, int month){
		if(month == FEBRUARY)
			if(day < MAX_DAY_IN_FEBRUARY) return true;
		if((month % TWO) == ONE)
			if(day < MAX_DAY_IN_UNEVEN_MONTHS) return true;
		if((month % TWO) == ZERO)
			if(day < MAX_DAY_IN_EVEN_MONTHS) return true;
		return false;
	}
}
