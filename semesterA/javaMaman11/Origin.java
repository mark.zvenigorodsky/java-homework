import java.util.Scanner;

/*
 * A class that accepts three coordinates of an X and Y and returns the closest point
 * to the origin
*/
 
public class Origin {
	public static void main (String [] args) {

		Scanner scan = new Scanner(System.in);
   
		System.out.println("Enter first point coordinate:");
		int x1 = scan.nextInt();
		int y1 = scan.nextInt();

		System.out.println("Enter second point coordinate:");
		int x2 = scan.nextInt();
		int y2 = scan.nextInt();
   
		System.out.println("Enter third point coordinate:");
		int x3 = scan.nextInt();
		int y3 = scan.nextInt();

		//Calculating the distance to the origin
 
		double point1 = (double) (Math.sqrt(Math.pow(x1,2) + Math.pow(y1,2)));
		double point2 = (double) (Math.sqrt(Math.pow(x2,2) + Math.pow(y2,2)));
		double point3 = (double) (Math.sqrt(Math.pow(x3,2) + Math.pow(y3,2)));

		//Checking for the shortest distance

		double closestPoint = point1;
		int closestX = x1;
		int closestY = y1;
   
		if(closestPoint > point2){
			closestPoint = point2;

			closestX = x2;
			closestY = y2;
		}
		if(closestPoint > point3){
			closestPoint = point3;

			closestX = x3;
			closestY = y3;
		}
	
		System.out.println("The nearest point to the origin is (" + closestX + "," + closestY 	+ ")");
	}
}
                                                            
