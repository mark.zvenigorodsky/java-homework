import java.util.Scanner;

/*
 * A class that accepts three coefficients of the polynomial equaiton 
 * and returns solutions if there are any
*/

public class Equation{
	public static void main(String [] args){
		
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter 3 coefficients of the polynomial equation:");
		double a = scan.nextDouble();
		double b = scan.nextDouble();
		double c = scan.nextDouble();

		//Calculating discriminant

		double discriminant = (double) (Math.pow(b,2)-(4*a*c));

		//Checking if there's a solution for the equation
		
		if(discriminant < 0){
			System.out.println("There is no solution.");
			return;	
		}
		
		//Solving for X

		double x1 = (double) ((-1 * b + Math.sqrt(discriminant)) / (2 * a));
		double x2 = (double) ((-1 * b - Math.sqrt(discriminant)) / (2 * a));

		//Checking if there's only one or two solutions

		if(x1 == x2){
			System.out.println("There is 1 solution. X1 = " + x1 + ".");
			return;
		}

		System.out.println("There are 2 solutions. X1 = " + x1 + ", X2 = " + x2 + ".");
	}
}
