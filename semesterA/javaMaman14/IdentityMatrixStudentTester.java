public class IdentityMatrixStudentTester {
    public static void printMatrix(int[][] mat) {
        for(int i = 0; i < mat.length; i++) {
            for(int j = 0; j < mat[i].length; j++)
                System.out.print(mat[i][j] + "\t");
            System.out.println();
        }
    }
    public static void main(String[] args) {
        int[][] A = {{1, 0, 0, 2, 0, 1, 2}, {0, 1, 0, 0, 0, 3, 0}, {0, 0, 1, 0, 0, 0, 0}, {5, 0, 0, 1, 0, 0, 0}, {7, 0, 0, 0, 1, 0, 0}, {8, 0, 0, 0, 0, 1, 0}, {1, 0, 0, 0, 0, 0, 0}};
        printMatrix(A);
        System.out.println("Is this matrix Identity Matrix: " + IdentityMatrix.isIdentity(A, 2, 3));    // expected true
        System.out.println("Is this matrix Identity Matrix: " + IdentityMatrix.isIdentity(A, 1, 5));    // expected false
        System.out.println("Max Identity Matrix size: " + IdentityMatrix.maxMatrix(A));    // expected 3
    }
}
