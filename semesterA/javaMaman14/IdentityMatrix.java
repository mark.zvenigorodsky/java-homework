/**
 * IdentityMatrix
 *
 * a class that answers the requirements of Maman 14
 *
 * @author Mark Zvenigorodsky id 327330684
 */
public class IdentityMatrix{

	/** 
	 * isIdentity
	 *
	 * A method to check if the matrix given is an identity matrix from a certain position to the left top corner and size
	 *
	 * @param mat, given matrix
	 * @param x, position in the matrix to check from
	 * @param size, size of the identity matrix to check for
	 */
	public static boolean isIdentity(int[][] mat, int x, int size){
		int[][] xMatrix = IdentityMatrix.newMatrix(mat,size,0);

		for(int i = 0; i < size; i++){
			if(!(IdentityMatrix.checkRow(xMatrix, i)) || size > xMatrix.length) return false;
		}
		return true;
	}
	
	/** 
	 * newMatrix
	 *
	 * a method to create a new matrix with a given start and end value
	 *
	 * @param mat, given matrix to copy 
	 * @param x, value to copy the matrix to
	 * @param from, value to start copying from 
	 */ 

	private static int[][] newMatrix(int[][] mat, int x, int from){
		int[][] newMatrix = new int[x][x];

		for(int i = from; i < x; i++){
			for(int j = from; j < x; j++){
				newMatrix[i - from][j - from] = mat[i][j];
			}
		}
		return newMatrix;
	}

	/** 
	 * checkRow
	 * 
	 * A method that checks if the given row inside an array has the correct placement to be considered an identity matrix
	 *
	 * @param mat, given matrix
	 * @param r, row and index to check agaist the row if the placement of the one is in the correct place
	 */
	private static boolean checkRow(int[][] mat, int r){
		int[] row = mat[r];

		for(int i = 0; i < row.length; i++){
			if(i == r && row[i] == 1) continue;
			if(row[i] == 0) continue;
			return false;
		}
		return true;
	}
	/** 
	 * maxMatrix 
	 *
	 * A method that takes a matrix and returns the size of the biggest central identity matrix it contains
	 *
	 * @param mat, matrix to check agaist
	 */
	public static int maxMatrix(int[][] mat){
		int minimalMatPosition= (mat.length + 1) / 2;
		int max = 0;
		for(int i = 1; i < mat.length; i += 2){
			int[][] newMatrix = IdentityMatrix.newMatrix(mat, mat.length, (mat.length - minimalMatPosition) - (i / 2));	
			if(IdentityMatrix.isIdentity(newMatrix, (i/2) , i)) max = i;
		}
		return max;
	}
}
