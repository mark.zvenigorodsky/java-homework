/**
 * SerpertineMatrix 
 *
 * a class with all of the requirements described in Maman ONE4
 *
 * @author Mark Zvenigorodsky id 327330684
 *
 */

public class SerpertineMatrix {
	/**
	 * checkRowAscending
	 *
	 * Check if the given line in a matrix is ascending as described in Maman 14, in a "serpentine way,
	 * from a given number.
	 *
	 * @param mat, matrix given
	 * @param r, which line to check 
	 * @param num, from which number inside the line to start the check from 
	 *
	 * @return true if the matrix is ascneding in a "serpentine way"
	 */

	
	public static boolean checkRowAscending(int[][] mat, int r, int num){
		final int ZERO = 0;
		final int ONE = 1;
		final int TWO = 2;

		int[] lineToCheck = new int[mat[r].length];

		for(int i = 0; i < mat[r].length; i++)lineToCheck = mat[r];

		boolean isCountingToRight = (r % TWO) == ZERO;
		int lineLength = lineToCheck.length - ONE;

		if(!isCountingToRight){
			int[] newArr = new int[lineLength + ONE];
			for(int i = lineLength; i >= ZERO; i--){
				newArr[lineLength - i] = lineToCheck[i];
			}
			for(int i = ZERO; i <= lineLength; i++){
				lineToCheck[i] = newArr[i];
			}
		}

		for(int i = ZERO; i <= lineLength; i++){
			if(lineToCheck[i] == num){
				int counter = num;
				for(int j = i; j <= lineLength; j++){
					if(lineToCheck[j] == counter && lineLength + ONE == mat[ZERO].length){
						if(j == lineLength) return true;
						counter++;
						continue;
					}
					return false;
				}
			}
		}
		return false;

	}

	/**
	 * checkRowDesecending
	 *
	 * Check if the given line in a matrix is descending as described in Maman 14, in a "serpentine way,
	 * to a given number.
	 *
	 * @param mat, matrix given
	 * @param r, which line to check 
	 * @param num, to which number inside the line to run the check to  
	 *
	 * @return true if the matrix is descending in a "serpentine way"
	 */

	public static boolean checkRowDesecending(int[][] mat, int r, int num){
		final int ZERO = 0;
		final int ONE = 1;
		final int TWO = 2;
		
		int[] lineToCheck = new int[mat[r].length];

		for(int i = 0; i < mat[r].length; i++)lineToCheck = mat[r];


		boolean isCountingToRight = (r % TWO) == ZERO;
		int positionOfNum;
		int lineLength = lineToCheck.length - ONE;

		if(isCountingToRight){
			int[] newArr= new int[lineLength + ONE];
			for(int i = lineLength; i >= ZERO; i--){
				newArr[lineLength - i] = lineToCheck[i];
			}
			for(int i = ZERO; i <= lineLength; i++){
				lineToCheck[i] = newArr[i];
			}
		}
		
		int counter = lineToCheck[ZERO];
		for(int j = ZERO; j <= lineLength; j++){
			if(lineToCheck[j] == counter){
				if(lineToCheck[j] == num) return true;
				counter--;
				continue;
			}
		}
		return false;
	}
	
	/**
	 * isSerpertine
	 *
	 * Check if the given matrix is arranged as described in Maman 14, in a "serpentine way"
	 * 
	 *
	 * @param mat, the matrix to check
	 *
	 * @return true if the matrix is arranged in a "serpentine way"
	 */

	public static boolean isSerpertine(int[][] mat){
		final int ZERO = 0;
		final int ONE = 1;
		final int TWO = 2;


		int columnLength = mat.length;
		int rowLength = mat[ZERO].length;

		for(int i = ZERO; i < columnLength; i++){
			int firstNum = mat[i][ZERO];
			
			if((i % TWO == ZERO && !checkRowAscending(mat, i, firstNum)) || !checkRowDesecending(mat, i, firstNum)) return false; 
		}
		return true;
	}
}
