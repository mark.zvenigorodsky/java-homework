public class SerpertineMatrixStudentTester {
    public static void printMatrix(int[][] mat) {
        for(int i = 0; i < mat.length; i++) {
            for(int j = 0; j < mat[i].length; j++)
                System.out.print(mat[i][j] + "\t");
            System.out.println();
        }
    }
    public static void main(String[] args) {
        int[][] mat1 = {{1, 2, 3, 4, 5}, {10, 9, 8, 7, 6}, {11, 12, 13, 14, 15}, {20, 19, 18, 17, 16}, {21, 22, 23, 24, 25}};
        int[][] mat2 = {{0, 2, 3, 4, 5}, {10, 9, 8, 7, 6}, {11, 12, 13, 14, 15}, {20, 19, 18, 17, 16}, {21, 22, 23, 24, 25}};
        int[][] mat3 = {{1, 2, 3, 4, 5}, {10, 9, 8, 7, 6}, {11, 12, 13, 15, 15}, {20, 19, 18, 17, 16}, {21, 22, 23, 24, 25}};
        int[][] mat4 = {{1, 2, 3, 4, 5}, {10, 9, 8, 7, 6}, {11, 12, 13, 14, 15}, {20, 19, 18, 17, 16}, {21, 22, 23, 24, 25, 26}};

        printMatrix(mat1);
        System.out.println("Is this matrix Serpertine Matrix: " + SerpertineMatrix.isSerpertine(mat1));
        printMatrix(mat2);
        System.out.println("Is this matrix Serpertine Matrix: " + SerpertineMatrix.isSerpertine(mat2));
        printMatrix(mat3);
        System.out.println("Is this matrix Serpertine Matrix: " + SerpertineMatrix.isSerpertine(mat3));
        printMatrix(mat4);
        System.out.println("Is this matrix Serpertine Matrix: " + SerpertineMatrix.isSerpertine(mat4));
			System.out.println(SerpertineMatrix.checkRowAscending(mat1, 0, 1));
	System.out.println(SerpertineMatrix.checkRowDesecending(mat1, 1, 1));
    }
}
