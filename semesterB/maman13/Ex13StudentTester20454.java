
public class Ex13StudentTester20454
{
    public static void main(String[] args){

        System.out.println("********** Question 1 **********\n");
        System.out.println("********** Question 2 **********\n");
        System.out.println();
        boolean[][] mat = {
                {false,false,false,false,true},
                {true,true,true,true,false},
                {true,false,true,true,false},
                {true,false,false,false,false},
                {true,true,false,false,false},
            };
        System.out.println ("Checking method cntTrueReg with the matrix:\n");
        printMat (mat);
        int studentCntRegResult=Ex13.cntTrueReg(mat);
        System.out.println();
        System.out.println("Result is: "+studentCntRegResult);

    }

    public static void printMat(boolean [][]mat){
        for (int i=0;i<mat.length;i++){
            for(int j=0;j<mat[0].length;j++)
                if(mat[i][j]== true)
                    System.out.print("1\t");
                else
                    System.out.print("0\t");
            System.out.println();
        }
    }
}

