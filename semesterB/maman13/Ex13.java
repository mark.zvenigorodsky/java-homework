/**
 * Ex13, a class that answers the requirements of maman 13
 *
 * in this exercise, only recursion can be used
 */
public class Ex13 {
    /** 
     * cntTrueReg
     *
     * A method that recieves a two dimensional array with boolean values and return the amount of 
     * "islands" that are made up from the matrixes positive values
     *
     * It does so by going through the matrix, counting each time it collides with a true position,
     * and iterates to all four directions from said position to "mark" the island and replaces its 
     * values to false so it won't count those positions again
     */
    public static int cntTrueReg(boolean[][] mat){
        int counter = 0; 
        
        counter = loopOverMatrix(mat, 0, 0, counter);
        return counter;
    }

    private static boolean[][] markIsland(boolean [][] mat, int i, int j){

        if( (i >= mat.length)|| 
            (j >= mat.length)|| 
            (j < 0) || 
            (i < 0) ||
            !mat[i][j]) return mat;
        
        mat[i][j] = false;

        markIsland(mat, i+1, j);
        markIsland(mat, i-1, j);
        markIsland(mat, i, j+1);
        markIsland(mat, i, j-1);

        return mat;
    } 

    private static int loopOverMatrix(boolean[][] mat, int i,int j,int counter){
        if(i >= mat.length) return counter;
        if(j >= mat.length) return loopOverMatrix(mat, i+1,0, counter);
        if(!mat[i][j]) return loopOverMatrix(mat, i ,j+1, counter);
                
        mat = markIsland(mat, i, j);
        counter++;
        return loopOverMatrix(mat, i, j+1, counter);
    }
}
