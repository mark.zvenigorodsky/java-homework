
/**
 * Ex11 
 *
 * A class that has the requested methods specified in Maman 11
 *
 * @author Mark Zvenigorodsky id 327330684
 */

public class Ex11{
	/**
	 * findSingle
	 *
	 * A static method to find in an array of number pairs a single number that doesn't have a pair
	 *
	 * Time Complexity: O(n)
	 * We iterate over the array only once
	 *
	 * Space Complexity: O(1)
	 * We only set the integer 'i' 
	 *
	 * The method finds the single integer by checking the previous and next index in the array
	 * if there's no match then we found the single integer inside the array.
	 *
	 * @param a , an array that contains integer pairs and only one integer without a pair 
	 *
	 * @return int, returns the found single integer
	 */
	public static int findSingle(int[] a){
		int single = 0;
		for(int i = 0; i < a.length; i++){
			if(i != 0 && a[i - 1] == a[i]) continue;
			if(i != a.length - 1 && a[i + 1] == a[i]) continue;
			single = a[i];
		}
		return single;
	}
	/**
	 * waterVolume
	 *
	 * A static method that calculates the amount of water that can be collected 
	 * inside a given array of integers
	 *
	 * Time Complexity: O(n)
	 * We iterate twice over the array which is still big O of 'n'
	 *
	 * Space Complexity: O(1)
	 * We set only six variables that are integeres
	 *
	 * On the first iteration the method checks for max height and sums up the collected water
	 * even if it doesn't reach another wall to block the water at the same height
	 * On the second iteration we iterate from the end of the array and sum the collected water
	 * as before until we reach the max height and remove the difference from the original sum.
	 *
	 * @param heights, the given array with integers to check
	 *
	 * @return sum, the sum of the water that can be collected in said array
	 *
	 */
	public static int waterVolume(int[] heights){
		int maxHeight = 0, localHeight = 0;
		int potentialSum = 0, sum = 0, difference = 0, finalSum = 0;
		for(int i = 0; i < heights.length; i++){
			if(heights[i] >= maxHeight){
				maxHeight= heights[i];
				potentialSum = 0;
				continue;
			}
			sum = sum + (maxHeight - heights[i]);
			potentialSum = potentialSum + (maxHeight - heights[i]);
		}
		for(int i = (heights.length - 1); i >= 0; i--){
			if(heights[i] == maxHeight){
				difference = potentialSum - difference;
				finalSum = sum - difference;
			}
			if(heights[i] >= localHeight){
				localHeight = heights[i];
				continue;
			}
			difference = difference + (localHeight - heights[i]);
 		}
		return finalSum;
	}
}
