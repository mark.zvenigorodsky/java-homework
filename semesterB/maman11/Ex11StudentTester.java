
public class Ex11StudentTester
{
    public static void main(String[] args){
        System.out.println ("********** Question 1 **********\n");

        int[] array1 = {6,6,18,18,-4,-4,12,9,9};
        System.out.println("Checking method findSingle on array [6,6,18,18,-4,-4,12,9,9]\n");
        int studentResult;

        studentResult= Ex11.findSingle(array1);
        System.out.println("Result is: "+studentResult);
        System.out.println();
        
        System.out.println ("********** Question 2 **********\n");
        int[] array2 = {2,1,1,4,1,1,2,3};
        System.out.println("Checking method waterVolume on array [2,1,1,4,1,1,2,3]\n");
        studentResult=Ex11.waterVolume(array2);
        System.out.println("Result is: "+studentResult);
        System.out.println();

        System.out.println ("********** Question 2 **********\n");
        int[] array3 = {4,1,1,2,1,1,3,3,3,1,2};
        System.out.println("Checking method waterVolume on array [2,1,1,4,1,1,2,3]\n");
        studentResult=Ex11.waterVolume(array3);
        System.out.println("Result is: "+studentResult);
        System.out.println();

        System.out.println ("********** Question 2 **********\n");
        int[] array4 = {4,5,6,7,8,-10,12,11,10,9,9};
        System.out.println("Checking method waterVolume on array [2,1,1,4,1,1,2,3]\n");
        studentResult=Ex11.waterVolume(array4);
        System.out.println("Result is: "+studentResult);
        System.out.println();

        System.out.println ("********** Question 2 **********\n");
        int[] array5 = {5,4,3,2,1};
        System.out.println("Checking method waterVolume on array [2,1,1,4,1,1,2,3]\n");
        studentResult=Ex11.waterVolume(array5);
        System.out.println("Result is: "+studentResult);
        System.out.println();

  
    }
}
      


