
/**
 * Ex12
 *
 * A class that has the requested methods specified in Maman 11
 *
 * @author Mark Zvenigorodsky id 327330684
 */
public class Ex12 {

	/**
	 * ascendingNum
	 *
	 * A static method to check if a given numbers digits are set in an ascending order
	 *
	 * The method checks if the first digit is bigger than the next occuring digit,
	 * and if so the method calls itself recursevly without the previous first digit
	 * 
	 * The method continues doing so until the given number is equal to its first digit,
	 * which means the numbers is set in an ascending order and the method returns true
	 *
	 * @param n, a given number to check if its set in an ascending order
	 *
	 * @return boolean, returns true if the number given is set in an ascending order
	 */  
	public static boolean ascendingNum(int n){
		int currentDigit = n % 10;		
		int nextNumber = (n / 10); 
		
		if(currentDigit == n) return true;
		if(currentDigit > (nextNumber % 10)) return Ex12.ascendingNum(nextNumber);
		return false;
	}

	/** 
	 * oddGCD
	 *
	 * a given method in the question
	 */

	private static int oddGCD(int m, int n){
		if(n == m) 
			return n;
		if(m > n)
			return oddGCD(n, m - n);
		return oddGCD(m, n - m);
	}
	
	/**
	 * generalGCD
	 *
	 * A static method that returns the greatest common factor (GCD) of two given numbers
	 *
	 * The method checks if the two numbers are even and if so it calls itself recursivly subtracting 
	 * the previous smaller number from the bigger one until it reaches the GCD, and if they are not then calls for the oddGCD method
	 * to calculate the GCD
	 *
	 * It does the same thing as the method oddGCD but checks for even numbers beforehand as the question asked for,
	 * the question was poorly described and the oddGCD method already does that for all kinds of numbers independently from the fact that they are even or not
	 * but it was stated in the question that the two cases needed to be seperate and make no other checks on the parameters
	 *
	 * @param m, first number of the two given
	 * @param n, second number of the two given
	 */
	public static int generalGCD(int m, int n){
		if(m % 2 == 0 && n % 2 == 0){
			if(n == m) 
				return n;
			if(m > n)
				return generalGCD(n, m - n);
			return generalGCD(m, n - m);
		}
		return Ex12.oddGCD(m, n);
	}

	/**
	 * f
	 *
	 * A static method that prints all of the substrings inside the given string, from the character 'a' to the end of the string
	 *
	 * The method checks in each recursive iteration if the current first character is 'a',
	 * if so it prints the current string and passes the next iteration the given string without the first character
	 *
	 * @param s, a given string to print its contents
	 */  
	public static void f(String s){
		String newString = s.substring(1);
		if(s.charAt(0) == 'a') System.out.println(s);	
		if(newString.length() == 1) return;
		Ex12.f(newString);
	}

}
