

public class ascendingNumTest{

	public static void test(int num){
		System.out.print(num + " ");
		if(Ex12.ascendingNum(num)){
			System.out.println("true");
			return;
		}
		System.out.println("false");
	}

	public static void main(String [] args){
		Ex12.test(123);
		Ex12.test(5);
		Ex12.test(0);
		Ex12.test(123432);
		Ex12.test(432);
		Ex12.test(589);
		Ex12.test(10);
		Ex12.test(01);
	}
}
