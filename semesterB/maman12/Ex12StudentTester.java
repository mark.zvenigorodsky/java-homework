
import java.util.Scanner;
public class Ex12StudentTester
{
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        System.out.println ("********** Question 1 **********\n");      
        System.out.println("Please enter an integer");
        int n = scan.nextInt();
        System.out.println("Checking method ascendingNum on number " + n +"\n");
        boolean studentResult1;
        studentResult1= Ex12.ascendingNum(n);
        System.out.println("Result is: "+studentResult1);
        System.out.println();

        System.out.println ("********** Question 2 **********\n");
        System.out.println("Please enter two positive integers");
        int a = scan.nextInt();
        int b = scan.nextInt();
        System.out.println("Checking method generalGCD on the numbers " 
                            + a +" and " + b +"\n");
        int studentResult2;
        studentResult2= Ex12.generalGCD(a, b);
        System.out.println("Result is: "+studentResult2);
        System.out.println();        

        System.out.println ("********** Question 3 **********\n");
        System.out.println("Please enter a string");
        String st = scan.next();
        System.out.println("Checking method f on the string " + st +"\n");        
        System.out.println("Result is: ");
        Ex12.f(st);
        System.out.println();
      
    }
}
