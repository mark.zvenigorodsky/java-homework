CLASSES:=$(shell ls)

.SUFFIXES: .java .class

.java.class:
	javac -g $*.java

default: classes 

classes: $(CLASSES:.java=.class)

execute: $(wildcard *.class)
	echo "something"

clean:
	rm -f *.class
